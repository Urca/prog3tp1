﻿using System;
using System.IO;

/*FileStream fs = File.Create("datosjuego.txt"); Creo un archivo fs con el File Stream
           StreamWriter sw = new StreamWriter(fs); Creo un stream sw que es el que trabaja sobre el archivo
           sw.WriteLine("Hola que tal"); Escribo sobre el archivo
           sw.Close(); Cierro el stream
           fs.Close(); Cierro el archivo*

           Console.WriteLine("Leyendo archivo");
           FileStream fs = File.OpenRead("datosjuego.txt"); 
           StreamReader sr = new StreamReader(fs); Lector
           string textoDeArchivo = sr.ReadLine(); Asigno la linea a un string
           Console.WriteLine(textoDeArchivo); Imprimo el string
           sr.Close();
           fs.Close();

           Console.WriteLine("Creando el archivo binario");
           FileStream fs = File.Create("datosJuegoBinario.txt");
           BinaryWriter bw = new BinaryWriter(fs);
           bw.Write(32);
           bw.Write(3.0f);             
           bw.Close();
           fs.Close();

           Console.WriteLine("Leyendo el archivo binario");
           FileStream fs = File.OpenRead("datosJuegoBinario.txt");
           BinaryReader = new BinaryReader(fs);
           int num = br.ReadSingle);
           int nume2 = br2.ReadInt32();
           br.Close();
           fs.Close();*/
namespace Prog3Tp1
{


    class Program
    {
        /* Couldn't finish on time
        static void Shoot(string lastKey)
        {
            switch (lastKey)
            {
                case "left":
                    break;
                case "right":
                    break;
                case "up":
                    break;
                case "down":
                    break;                    
            }
        }*/

        static void Menu(ref bool gameRunning1, ref bool gameRunning2, ref bool gamePlaying)
        {


            int menu = 0;

            Console.WriteLine("1. Un Jugador");
            Console.WriteLine("2. Dos jugadores");
            Console.WriteLine("3. Salir");
            menu = Convert.ToInt16(Console.ReadLine());

            if (menu == 1)
            {
                gameRunning1 = true;
            }
            else if (menu == 2) //agregue la opcion de dos jugadores
            {
                gameRunning2 = true;
            }
            if (menu == 3)
            {
                gamePlaying = false;
            }

        }
       
        static int Jugar(ref bool gameRunning1, ref bool gameRunning2)
        {

            ConsoleKeyInfo userKey;

            Enemigo ene1 = new Enemigo();
            Enemigo ene2 = new Enemigo();
            Enemigo ene3 = new Enemigo();
            //Player 1 positions
            const int initLocationX = 39;
            const int initLocationY = 12;
            int locationX = initLocationX;
            int locationY = initLocationY;

            //Player 2 positions
            const int initLocationX2 = 39;
            const int initLocationY2 = 10;
            int locationX2 = initLocationX2;
            int locationY2 = initLocationY2;

            //Welcome
            string key = "";

            //Enemy position
            Random rnd = new Random();
            int bomb1LocX = rnd.Next(1, 78);
            int bomb1LocY = rnd.Next(1, 24);
            int bomb2LocX = rnd.Next(1, 78);
            int bomb2LocY = rnd.Next(1, 24);
            int bomb3LocX = rnd.Next(1, 78);
            int bomb3LocY = rnd.Next(1, 24);
            int item1LocX = rnd.Next(1, 78);
            int item1LocY = rnd.Next(1, 24);
            int item2LocX = rnd.Next(1, 78);
            int item2LocY = rnd.Next(1, 24);
            int item3LocX = rnd.Next(1, 78);
            int item3LocY = rnd.Next(1, 24);

            int score = 0;

            //Lives
            int numVidas = 1;
            char charVida = '♥';

            //Shoot
            string lastKeyPressed;

            /*while (gameRunning2) //caso dos jugadores
            {
                if (Console.KeyAvailable)
                {
                    userKey = Console.ReadKey(true);

                    switch (userKey.Key)
                    {
                        case ConsoleKey.LeftArrow:
                            if (locationX > 0) locationX = locationX - 1;
                            lastKeyPressed = "left";
                            break;
                        case ConsoleKey.RightArrow:
                            if (locationX < 78) locationX = locationX + 1;
                            lastKeyPressed = "right";
                            break;
                        case ConsoleKey.UpArrow:
                            if (locationY > 0) locationY = locationY - 1;
                            lastKeyPressed = "up";
                            break;
                        case ConsoleKey.DownArrow:
                            if (locationY < 24) locationY = locationY + 1;
                            lastKeyPressed = "down";
                            break;
                        case ConsoleKey.Escape:
                            gameRunning1 = false;
                            break;
                    }
                    //movimiento player 2
                    key = Console.ReadKey().KeyChar.ToString();
                    if (key == "w" && locationY2 > 0 || key == "W" && locationY2 > 0)
                    {
                        locationY2 -= 1;
                    }
                    if (key == "s" && locationY2 < 24 || key == "S" && locationY2 < 24)
                    {
                        locationY2 += 1;
                    }
                    if (key == "a" && locationX2 > 0 || key == "A" && locationX2 > 0)
                    {
                        locationX2 -= 1;
                    }
                    if (key == "d" && locationX2 < 78 || key == "D" && locationX2 < 78)
                    {
                        locationX2 += 1;
                    }
                    //
                }

                if (locationX == ene1LocX && locationY == ene1LocY)
                {
                    gameRunning2 = false;
                }
                else if (locationX == ene2LocX && locationY == ene2LocY)
                {
                    gameRunning2 = false;
                }
                else if (locationX == ene3LocX && locationY == ene3LocY)
                {
                    gameRunning2 = false;
                }

                //muerte player 2

                if (locationX2 == ene1LocX && locationY2 == ene1LocY)
                {
                    gameRunning2 = false;
                }
                else if (locationX2 == ene2LocX && locationY2 == ene2LocY)
                {
                    gameRunning2 = false;
                }
                else if (locationX2 == ene3LocX && locationY2 == ene3LocY)
                {
                    gameRunning2 = false;
                }
                //
                Console.Clear();
                Console.SetCursorPosition(locationX, locationY);
                Console.Write("@");
                //escritura en pantalla player 2
                Console.SetCursorPosition(locationX2, locationY2);
                Console.Write("0");
                //
                Console.SetCursorPosition(ene1LocX, ene1LocY);
                Console.Write("%");
                Console.SetCursorPosition(ene2LocX, ene2LocY);
                Console.Write("&");
                Console.SetCursorPosition(ene3LocX, ene3LocY);
                Console.Write("*");

                System.Threading.Thread.Sleep(50);
            }*/

            while (gameRunning1) //juego original. Solo cambie el nombre del bool
            {
                if (Console.KeyAvailable)
                {
                    userKey = Console.ReadKey(true);

                    switch (userKey.Key)
                    {
                        case ConsoleKey.LeftArrow:
                            if (locationX > 0) locationX = locationX - 1;
                            lastKeyPressed = "left";
                            break;
                        case ConsoleKey.RightArrow:
                            if (locationX < 78) locationX = locationX + 1;
                            lastKeyPressed = "right";
                            break;
                        case ConsoleKey.UpArrow:
                            if (locationY > 0) locationY = locationY - 1;
                            lastKeyPressed = "up";
                            break;
                        case ConsoleKey.DownArrow:
                            if (locationY < 24) locationY = locationY + 1;
                            lastKeyPressed = "down";
                            break;
                        case ConsoleKey.Escape:
                            gameRunning1 = false;
                            break;
                        case ConsoleKey.Spacebar:
                            //Shoot();//Shoot bullet
                            break;
                    }

                }

                
                if (locationX == item1LocX && locationY == item1LocY)
                {
                    score++;
                    Console.SetCursorPosition(initLocationX, initLocationY);
                    Console.Write("@");
                    locationX = initLocationX;
                    locationY = initLocationY;
                    Console.SetCursorPosition(bomb1LocX, bomb1LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(bomb2LocX, bomb2LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(bomb3LocX, bomb3LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(item1LocX, item1LocY);
                    Console.Write("I");
                    Console.SetCursorPosition(item2LocX, item2LocY);
                    Console.Write("I");
                    Console.SetCursorPosition(item3LocX, item3LocY);
                    Console.Write("I");

                }
                if (locationX == item2LocX && locationY == item2LocY)
                {
                    score++;
                    Console.SetCursorPosition(initLocationX, initLocationY);
                    Console.Write("@");
                    locationX = initLocationX;
                    locationY = initLocationY;
                    Console.SetCursorPosition(bomb1LocX, bomb1LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(bomb2LocX, bomb2LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(bomb3LocX, bomb3LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(item1LocX, item1LocY);
                    Console.Write("I");
                    Console.SetCursorPosition(item2LocX, item2LocY);
                    Console.Write("I");
                    Console.SetCursorPosition(item3LocX, item3LocY);
                    Console.Write("I");

                }
                if (locationX == item3LocX && locationY == item3LocY)
                {
                    score++;
                    Console.SetCursorPosition(initLocationX, initLocationY);
                    Console.Write("@");
                    locationX = initLocationX;
                    locationY = initLocationY;
                    Console.SetCursorPosition(bomb1LocX, bomb1LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(bomb2LocX, bomb2LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(bomb3LocX, bomb3LocY);
                    Console.Write("B");
                    Console.SetCursorPosition(item1LocX, item1LocY);
                    Console.Write("I");
                    Console.SetCursorPosition(item2LocX, item2LocY);
                    Console.Write("I");
                    Console.SetCursorPosition(item3LocX, item3LocY);
                    Console.Write("I");

                }

                if (locationX == bomb1LocX && locationY == bomb1LocY )
                {
                    if (numVidas > 0)
                    {
                        numVidas--;//Lose a life, restart scene
                        Console.Clear();
                        Console.SetCursorPosition(initLocationX, initLocationY);
                        Console.Write("@");
                        locationX = initLocationX;
                        locationY = initLocationY;
                        Console.SetCursorPosition(bomb1LocX, bomb1LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(bomb2LocX, bomb2LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(bomb3LocX, bomb3LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(item1LocX, item1LocY);
                        Console.Write("I");
                        Console.SetCursorPosition(item2LocX, item2LocY);
                        Console.Write("I");
                        Console.SetCursorPosition(item3LocX, item3LocY);
                        Console.Write("I");

                    }
                    else
                    {
                        //No lifes = no game
                        gameRunning1 = false;

                    }
                }
                else if (locationX == bomb2LocX && locationY == bomb2LocY )
                {
                    if (numVidas > 0)
                    {
                        numVidas--;
                        Console.Clear();
                        Console.SetCursorPosition(initLocationX, initLocationY);
                        Console.Write("@");
                        locationX = initLocationX;
                        locationY = initLocationY;
                        Console.SetCursorPosition(bomb1LocX, bomb1LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(bomb2LocX, bomb2LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(bomb3LocX, bomb3LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(item1LocX, item1LocY);
                        Console.Write("I");
                        Console.SetCursorPosition(item2LocX, item2LocY);
                        Console.Write("I");
                        Console.SetCursorPosition(item3LocX, item3LocY);
                        Console.Write("I");

                    }
                    else
                    {
                        gameRunning1 = false;

                    }
                }
                else if (locationX == bomb3LocX && locationY == bomb3LocY )
                {
                    if (numVidas > 0)
                    {
                        numVidas--;
                        Console.Clear();
                        Console.SetCursorPosition(initLocationX, initLocationY);
                        Console.Write("@");
                        locationX = initLocationX;
                        locationY = initLocationY;
                        Console.SetCursorPosition(bomb1LocX, bomb1LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(bomb2LocX, bomb2LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(bomb3LocX, bomb3LocY);
                        Console.Write("B");
                        Console.SetCursorPosition(item1LocX, item1LocY);
                        Console.Write("I");
                        Console.SetCursorPosition(item2LocX, item2LocY);
                        Console.Write("I");
                        Console.SetCursorPosition(item3LocX, item3LocY);
                        Console.Write("I");

                    }
                    else
                    {

                        gameRunning1 = false;

                    }
                }


                Console.Clear();
                ene1.crear();
                ene1.mover();
                ene2.crear();
                ene2.mover();
                ene3.crear();
                ene3.mover();
                Console.SetCursorPosition(locationX, locationY);
                Console.Write("@");
                Console.SetCursorPosition(bomb1LocX, bomb1LocY);
                Console.Write("B");
                Console.SetCursorPosition(bomb2LocX, bomb2LocY);
                Console.Write("B");
                Console.SetCursorPosition(bomb3LocX, bomb3LocY);
                Console.Write("B");
                Console.SetCursorPosition(item1LocX, item1LocY);
                Console.Write("I");
                Console.SetCursorPosition(item2LocX, item2LocY);
                Console.Write("I");
                Console.SetCursorPosition(item3LocX, item3LocY);
                Console.Write("I");
                Console.SetCursorPosition(0, 0);

                Console.Write("" + numVidas + "" + charVida);
                Console.Write("Score=" + "" + score + "");

                System.Threading.Thread.Sleep(80);


            }


            return score;


        }


        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;//To be able to see the heart
            Console.CursorVisible = false;//Because it's annoying

            if (File.Exists("Bienvenida.txt"))
            {
                FileStream fs = File.OpenRead("Bienvenida.txt");
                StreamReader sr = new StreamReader(fs);
                Console.WriteLine(sr.ReadLine());
                sr.Close();
                fs.Close();
            }
            else
            {
                FileStream fs = File.Create("Bienvenida.txt");
                StreamWriter sw = new StreamWriter(fs);
                Console.WriteLine("Escriba su mensaje de bienvenida");
                sw.WriteLine(Console.ReadLine());
                sw.Close();
                fs.Close();
            }
            if (File.Exists("DatosGameOver.bin"))
            {
                FileStream fs4 = File.OpenRead("DatosGameOver.bin");
                BinaryReader br3 = new BinaryReader(fs4);
                string nombretxt = br3.ReadString();
                int scoretxt = br3.ReadInt32();
                Console.WriteLine("El Mayor Puntaje es " + scoretxt + " y lo realizo " + nombretxt);
                br3.Close();
                fs4.Close();

            }

            bool gamePlaying = true;
            bool gameRunning1 = false;
            bool gameRunning2 = false;//cree un  nuevo bool para la opcion de dos jugadores
            string nombre;



            while (gamePlaying == true)
            {
                Menu(ref gameRunning1, ref gameRunning2, ref gamePlaying); //envio un doble bool

                int score = Jugar(ref gameRunning1, ref gameRunning2);



                if (gameRunning1 == false && gameRunning2 == false && gamePlaying == true)
                {



                    if (File.Exists("DatosGameOver.bin"))
                    {
                        Console.Clear();
                        FileStream fs2 = File.OpenRead("DatosGameOver.bin");
                        BinaryReader br2 = new BinaryReader(fs2);
                        string nombretxt = br2.ReadString();
                        int scoretxt = br2.ReadInt32();
                        br2.Close();
                        fs2.Close();
                        if (score > scoretxt)
                        {
                            Console.Clear();
                            Console.WriteLine("Felicitaciones, haz logrado la mayor puntuacion");
                            Console.WriteLine("Escriba Su nombre");
                            nombre = Convert.ToString(Console.ReadLine());
                            Console.WriteLine("Felicitacion " + nombre + " tu puntaje es de " + score);
                            FileStream fs3 = File.OpenWrite("DatosGameOver.bin");
                            BinaryWriter bw2 = new BinaryWriter(fs3);
                            bw2.Write(nombre);
                            bw2.Write(score);
                            bw2.Close();
                            fs3.Close();
                            Console.ReadKey();
                            Console.Clear();
                            Console.WriteLine("Game Over");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else
                        {
                            Console.WriteLine("Game Over");
                            Console.WriteLine("Tu puntaje es de " + score);
                            Console.ReadKey();
                            Console.Clear();

                        }

                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Game Over");
                        Console.WriteLine("Escriba Su nombre");
                        nombre = Convert.ToString(Console.ReadLine());
                        Console.WriteLine(nombre + " tu puntaje es de " + score);
                        Console.ReadKey();
                        Console.Clear();
                        FileStream fs = File.Create("DatosGameOver.bin");
                        BinaryWriter bw = new BinaryWriter(fs);
                        bw.Write(nombre);
                        bw.Write(score);
                        bw.Close();
                        fs.Close();

                    }

                }
            }

        }
    }
}

